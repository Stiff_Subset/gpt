from torch.nn.functional import scaled_dot_product_attention

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F


class MultiheadAttention(nn.Module):
    def __init__(self, embed_dim, n_heads, dropout):
        super().__init__()
        assert embed_dim % n_heads == 0

        self.embed_dim = embed_dim
        self.n_heads = n_heads
        self.dropout_p = dropout

        self.layer_norm = nn.LayerNorm(embed_dim)
        self.attn_l = nn.Linear(embed_dim, 3 * embed_dim)
        self.proj_l = nn.Linear(embed_dim, embed_dim)
        self.dropout = nn.Dropout(dropout)
        self._reset_parameters()

    def _reset_parameters(self):
        nn.init.normal_(self.attn_l.weight, 0, 0.02)
        nn.init.xavier_normal_(self.proj_l.weight)  

    def forward(self, x):
        B, L, D = x.shape

        residual = x
        x = self.layer_norm(x)

        q, k, v = self.attn_l(x).split(self.embed_dim, dim=2)
        q = q.view(B, L, self.n_heads, D // self.n_heads).transpose(1, 2)
        k = k.view(B, L, self.n_heads, D // self.n_heads).transpose(1, 2)
        v = v.view(B, L, self.n_heads, D // self.n_heads).transpose(1, 2)
        attn = scaled_dot_product_attention(q, k, v, is_causal=True, dropout_p=self.dropout_p)

        output = attn.transpose(1, 2).contiguous().view(B, L, D)
        output = self.proj_l(output)
        output = self.dropout(output + residual)

        return output


class FeedForwardNetwork(nn.Module):
    def __init__(self, embed_dim, hidden_dim, dropout):
        super().__init__()

        self.layer_norm = nn.LayerNorm(embed_dim)
        self.proj_l1 = nn.Linear(embed_dim, hidden_dim)
        self.proj_l2 = nn.Linear(hidden_dim, embed_dim)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        residual = x

        output = self.layer_norm(x)
        output = self.proj_l1(output)
        output = F.relu(output)
        output = self.proj_l2(output)
        output = self.dropout(output + residual)

        return output


class TransformerBlock(nn.Module):
    def __init__(self, embed_dim, n_heads, ffn_dim, dropout):
        super().__init__()

        self.self_attn = MultiheadAttention(embed_dim, n_heads, dropout)
        self.ffn = FeedForwardNetwork(embed_dim, ffn_dim, dropout)

    def forward(self, x):
        output = self.self_attn(x)
        output = self.ffn(output)

        return output
    

class GPT(nn.Module):
    def __init__(
        self,
        vocab_size, pad_id, max_seq_len,
        n_layers, embed_dim, n_heads, ffn_dim, dropout,
    ):
        super().__init__()

        self.max_seq_length = max_seq_len

        self.tkn_embeds = nn.Embedding(vocab_size, embed_dim, padding_idx=pad_id)
        self.pos_embeds = nn.Embedding(max_seq_len, embed_dim)
        self.dropout = nn.Dropout(dropout)
        self.transformer = nn.ModuleList(
            [
                TransformerBlock(embed_dim, n_heads, ffn_dim, dropout)
                for _ in range(n_layers)
            ]
        )
        self.layer_norm = nn.LayerNorm(embed_dim)
        self.fc = nn.Linear(embed_dim, vocab_size)

    def forward(self, tokens):
        output = self.tkn_embeds(tokens) + self.pos_embeds(
            torch.arange(tokens.shape[1]).unsqueeze(0).expand(tokens.shape[0], -1).to(tokens.device)
        )
        output = self.dropout(output)

        for block in self.transformer:
            output = block(output)
        output = self.layer_norm(output)
        output = self.fc(output)

        return output
    
    @torch.inference_mode()
    def inference(self, idx, tokenizer, device, max_gen_len=256):
        self.eval()
        idx = torch.cat((torch.tensor([[tokenizer.bos_id]], dtype=torch.long).to(device), idx), dim=1)
        while idx.shape[1] < max_gen_len:
            if idx.shape[1] > self.max_seq_length:
                cur_idx = idx[:, -self.max_seq_length:]
            else:
                cur_idx = idx
            
            logits = self(cur_idx)
            probs = F.softmax(logits, dim=-1)[:, -1, :].squeeze(0).cpu().numpy()
            next_token_id = np.random.choice(len(probs), 1, p=probs)[0]
            if next_token_id == tokenizer.eos_id:
                break
            next_token = torch.tensor([[next_token_id]], dtype=torch.long).to(device)
            idx = torch.cat((idx, next_token), dim=1)
        return idx

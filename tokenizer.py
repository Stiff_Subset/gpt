from utils import ROOT_PATH
from sentencepiece import SentencePieceTrainer, SentencePieceProcessor


class Tokenizer:
    def __init__(self, model_prefix, vocab_size):
        model_path = ROOT_PATH / (model_prefix + ".model")
        if not model_path.exists():
            SentencePieceTrainer.train(
                input=str(ROOT_PATH / "train.txt"),
                model_type="bpe",
                input_sentence_size=600000,
                shuffle_input_sentence=True,
                vocab_size=vocab_size,
                model_prefix=model_prefix,
                normalization_rule_name="nmt_nfkc",
                pad_id=0, pad_piece="<PAD>",
                unk_id=1, unk_piece="<UNK>",
                bos_id=2, bos_piece="<BOS>",
                eos_id=3, eos_piece="<EOS>",
                minloglevel=1,
            )
        
        self.pad_id = 0
        self.unk_id = 1
        self.bos_id = 2
        self.eos_id = 3
        self.vocab_size = vocab_size

        self.sp_model = SentencePieceProcessor(model_file=str(ROOT_PATH / (model_prefix + ".model")))

    def encode(self, texts):
        return self.sp_model.encode(texts)

    def decode(self, tokens):
        return self.sp_model.decode(tokens)

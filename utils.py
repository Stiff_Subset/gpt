from itertools import repeat
from pathlib import Path
from tqdm import tqdm

import os


ROOT_PATH = Path(__file__).absolute().resolve().parent
TEXTS_URL = {
    "train": "https://huggingface.co/datasets/roneneldan/TinyStories/resolve/main/TinyStoriesV2-GPT4-train.txt?download=true",
    "valid": "https://huggingface.co/datasets/roneneldan/TinyStories/resolve/main/TinyStoriesV2-GPT4-valid.txt?download=true",
}


def preprocess_texts():
    for split, url in TEXTS_URL.items():
        if (ROOT_PATH / (split + ".txt")).exists():
            continue
        unprocessed = f"{split}_unprocessed.txt"
        processed = f"{split}.txt"
        os.system(f"wget {url} -O {split}_unprocessed.txt")
        with open(unprocessed) as u:
                unprocessed_texts = u.read().split("<|endoftext|>")
                processed_texts = []
                for text in unprocessed_texts:
                    text = text.strip()
                    text = " ".join(text.split("\n"))
                    processed_texts.append(text)

                with open(processed, "w") as p: 
                    for i, text in enumerate(processed_texts):
                        p.write(text)
                        if i + 1 != len(processed_texts):
                            p.write("\n")
        os.system(f"rm {split}_unprocessed.txt")


def load_texts(split, tokenizer, max_seq_len):
    tokenized_texts = []
    split_len = max_seq_len - 2
    with (ROOT_PATH / (split + ".txt")).open() as f:
        lines = f.readlines()
        for i in tqdm(range(0, len(lines), 64), desc=f"Tokenization of {split} texts"):
            tokenized = tokenizer.encode(lines[i:i + 64])
            for text in tokenized:
                for j in range(0, len(text), split_len):
                    tokenized_texts.append([tokenizer.bos_id] + text[j:j + split_len] + [tokenizer.eos_id])
        return tokenized_texts


def inf_loop(data_loader):
    """
    Wrapper function for endless data loader
    """
    
    for loader in repeat(data_loader):
        yield from loader

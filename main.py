from model import GPT
from tokenizer import Tokenizer
from trainer import train
from utils import load_texts, preprocess_texts

from torch.utils.data import DataLoader

import json
import torch
import torch.nn.functional as F


def collate_fn(batch):
    max_length = max(len(tokens) for tokens in batch)
    tokens = [
        F.pad(torch.tensor(tokens, dtype=torch.long), (0, max_length - len(tokens))) 
        for tokens in batch
    ]
    return torch.stack(tokens)


torch.backends.cudnn.deterministic = False
torch.backends.cudnn.benchmark = True

with open("config.json") as f:
    config = json.load(f)

device = "cuda" if torch.cuda.is_available() else "cpu"
tokenizer = Tokenizer("tokenizer", config["vocab_size"])

preprocess_texts()
train_texts = load_texts("train", tokenizer, config["max_seq_len"])
valid_texts = load_texts("valid", tokenizer, config["max_seq_len"])

train_loader = DataLoader(
    train_texts,
    collate_fn=collate_fn,
    batch_size=config["batch_size"],
    shuffle=True,
    num_workers=28,
    pin_memory=True,
)
valid_loader = DataLoader(
    valid_texts,
    collate_fn=collate_fn,
    batch_size=config["batch_size"],
    shuffle=False,
    num_workers=28,
    pin_memory=True,
)

model = GPT(
    config["vocab_size"],
    tokenizer.pad_id,
    config["max_seq_len"],
    **config["transformer_config"]
).to(device)

train(model, tokenizer, train_loader, valid_loader, config["train_config"], device)

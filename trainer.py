from tqdm import tqdm
from utils import inf_loop

import torch
import wandb


@torch.inference_mode()
def get_grad_norm(model, norm_type=2):
    parameters = model.parameters()
    if isinstance(parameters, torch.Tensor):
        parameters = [parameters]
    parameters = [p for p in parameters if p.grad is not None]
    total_norm = torch.norm(
        torch.stack(
            [torch.norm(p.grad.detach(), norm_type).cpu() for p in parameters]
        ),
        norm_type,
    )
    return total_norm.item()

    
def train(model, tokenizer, train_loader, valid_loader, config, device):
    train_loader = inf_loop(train_loader)

    optimizer = torch.optim.Adam(
        model.parameters(),
        lr=config["lr"],
        betas=config["betas"],
    )
    scheduler = torch.optim.lr_scheduler.OneCycleLR(
        optimizer,
        config["lr"],
        epochs=config["epochs"],
        steps_per_epoch=config["steps_per_epoch"],
        pct_start=config["scheduler_pct_start"],
    )
    criterion = torch.nn.CrossEntropyLoss(
        ignore_index=tokenizer.pad_id,
        label_smoothing=0.075,
    )

    wandb.init(project="GPT")
    prefixes = [
        "Once upon a time there was a grandfather and a grandmother",
        "In country far, and days long gone, there lived a famous Tsar Dadon.",
        "One morning, as Gregor Samsa was waking up from anxious dreams, he discovered that in bed he had been changed into a monstrous verminous bug.",
        "At the sunset hour of one warm spring day two men were to be seen at Patriarch's Ponds.",
        "Ten little nigger boys went out to dine. One choked his little self, and then there were nine."
    ]
    
    step = 0
    best_val_loss = 1e20
    with torch.backends.cuda.sdp_kernel(enable_flash=True, enable_math=False, enable_mem_efficient=False):
        for epoch in range(1, config["epochs"] + 1):
            model.train()
            train_loss = 0.0
            num_sentences = 0
            for i, tokens in enumerate(
                tqdm(train_loader, desc=f"Training {epoch} epoch", total=config["steps_per_epoch"])
            ):
                tokens = tokens.to(device)
                src = tokens[:, :-1]
                tgt = tokens[:, 1:]

                with torch.autocast(device_type=device, dtype=torch.bfloat16):
                    logits = model(src)
                    loss = criterion(logits.reshape(-1, logits.shape[-1]), tgt.reshape(-1))
                optimizer.zero_grad()
                loss.backward()

                optimizer.step()
                scheduler.step()

                train_loss += loss.item() * tokens.shape[0]
                num_sentences += tokens.shape[0]
                
                if i % config["log_steps"] == 0:
                    step = (epoch - 1) * config["steps_per_epoch"] + i
                    wandb.log(
                        {
                            "train_loss": train_loss / num_sentences,
                            "learning rate": scheduler.get_last_lr()[0],
                            "grad_norm": get_grad_norm(model),
                        },
                        step=step,
                    )

                if i + 1 == config["steps_per_epoch"]:
                    break
            train_loss /= num_sentences

            model.eval()
            valid_loss = 0.0
            num_sentences = 0
            with torch.inference_mode():
                for tokens in tqdm(
                    valid_loader, desc=f"Validating {epoch} epoch", total=len(valid_loader)
                ):
                    tokens = tokens.to(device)
                    src = tokens[:, :-1]
                    tgt = tokens[:, 1:]

                    with torch.autocast(device_type=device, dtype=torch.bfloat16):
                        logits = model(src)
                        loss = criterion(logits.reshape(-1, logits.shape[-1]), tgt.reshape(-1))

                    valid_loss += loss.item() * tokens.shape[0]
                    num_sentences += tokens.shape[0]
            valid_loss /= num_sentences

            pred_table = wandb.Table(columns=["prefix", "generated_text"])
            for prefix in tqdm(prefixes, desc=f"Generating {epoch} epoch"):
                with torch.autocast(device_type=device, dtype=torch.bfloat16):
                    tokens = model.inference(
                        torch.tensor([tokenizer.encode(prefix)], dtype=torch.long).to(device),
                        tokenizer,
                        device,
                        max_gen_len=100,
                    ).cpu().squeeze(0)
                text = tokenizer.decode(tokens.tolist())
                pred_table.add_data(prefix, text)

            wandb.log(
                {
                    "valid_loss": valid_loss,
                    "predictions": pred_table,
                },
                step=step,
            )

            if best_val_loss > valid_loss:
                best_val_loss = valid_loss
                state = {
                    "epoch": epoch,
                    "state_dict": model.state_dict(),
                    "optimizer": optimizer.state_dict(),
                    "scheduler": scheduler.state_dict(),
                }
                torch.save(state, "best_model.pth")
